# Task Escalator

[![coverage report](https://gitlab.com/benscrane/task-escalator/badges/master/coverage.svg)](https://gitlab.com/benscrane/task-escalator/-/commits/master)
[![pipeline status](https://gitlab.com/benscrane/task-escalator/badges/master/pipeline.svg)](https://gitlab.com/benscrane/task-escalator/-/commits/master)

An extension for Todoist.

## Setup

```bash
> npm install
> npm install --prefix ./functions
> npm run build
```


## Local Development

This won't run Firebase functions in development mode, just the front end Vue app.

```bash
> npm run serve
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
